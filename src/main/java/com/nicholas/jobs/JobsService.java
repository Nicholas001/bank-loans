package com.nicholas.jobs;

import com.nicholas.db.DbUtil;

import java.sql.*;
import java.util.Scanner;

public class JobsService {

    public void displayAllJobs()throws SQLException {

        Connection connection = DbUtil.getConnection();
        String quary = "SELECT * FROM jobs;";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(quary);
        System.out.println("Jobs:\n");
        while (resultSet.next()) {
            int jobID = resultSet.getInt("Job_ID");
            String title = resultSet.getString("Title");
            int income = resultSet.getInt("Income");
            System.out.println("\t" + jobID + ": " + title + " - " + income + ";\n");
        }
        statement.close();
        connection.close();
    }

    public void insertNewJob() throws SQLException {

        Connection connection = DbUtil.getConnection();
        String quary = "SELECT job_id FROM jobs ORDER BY job_id DESC LIMIT 1;";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(quary);
        int job_id=0;
        while (resultSet.next()) {
            job_id = resultSet.getInt("job_ID");}
        job_id++;
        statement.close();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please insert data for the new job: ");

        System.out.println("\t Job title:");
        String title = scanner.nextLine();

        System.out.println("\t Income: ");
        int income = scanner.nextInt();

        String sql = "INSERT INTO jobs (job_id, title, income ) VALUES (?, ?, ? )";
        PreparedStatement statement2 = connection.prepareStatement(sql);

        statement2.setInt(1, job_id);
        statement2.setString(2, title);
        statement2.setInt(3, income);

        statement2.execute();

        statement2.close();
        connection.close();
        System.out.println("The new job has been added!");
    }

    public void deleteJob() throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which job do you what to remove? Plase insert job id: ");
        int id = scanner.nextInt();

        Connection connection = DbUtil.getConnection();
        String sql = "DELETE FROM jobs WHERE (Job_ID = ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, id);

        statement.execute();

        statement.close();
        connection.close();

        System.out.println("Job " + id + " was deleted! \n");
    }


    public void updateJob() throws SQLException {
        Connection connection = DbUtil.getConnection();
        System.out.println("Please insert job id: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        scanner.nextLine();

        System.out.println("What do you want to change\n");
        System.out.println("\t1. Title \n\t2. Income \n");
        int option = scanner.nextInt();
        scanner.nextLine();
        switch (option) {
            case 1:
                String sql1 = "UPDATE jobs SET Title = ? WHERE (Job_ID = " + id + ");\n ";
                PreparedStatement statement1 = connection.prepareStatement(sql1);
                System.out.println("Plase enter the new title ");
                String title = scanner.nextLine();
                statement1.setString(1, title);
                statement1.execute();
                statement1.close();
                break;
            case 2:
                String sql2 = "UPDATE jobs SET Income = ? WHERE (Job_ID = " + id + ");\n ";
                PreparedStatement statement2 = connection.prepareStatement(sql2);
                System.out.println("Plase enter the new income ");
                int income = scanner.nextInt();
                statement2.setInt(1, income);
                statement2.execute();
                statement2.close();
                break;
            case 3:
                break;
            default:
                System.out.println("The selected option doesn`t exist! \n");
        }
        connection.close();
    }


}
