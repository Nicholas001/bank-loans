package com.nicholas.jobs;

import java.sql.SQLException;
import java.util.Scanner;

public class JobsMeniu {

    private JobsService service;

    public JobsMeniu() {
        service = new JobsService();
    }


    public void jobsMeniu() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        boolean test = true;

        while (test) {
            System.out.println("Please insert:\n" +
                    "\t 1. Display all jobs\n" +
                    "\t 2. Insert new job\n" +
                    "\t 3. Delete job\n" +
                    "\t 4. Update job\n" +
                    "\t 5. Return \n");

            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    displatAllJob();
                    break;
                case 2:
                    insertNewJob();
                    break;
                case 3:
                    deleteJob();
                    break;
                case 4:
                    updateJob();
                    break;
                case 5:
                    test = false;
                    break;
                default:
                    System.out.println("The selected option doesn`t exist! \n");
            }
        }
    }

    public void displatAllJob() throws SQLException {
        service.displayAllJobs();
    }

    public void insertNewJob() throws SQLException {
        service.insertNewJob();
    }

    public void deleteJob() throws SQLException {
        service.deleteJob();
    }

    public void updateJob() throws SQLException {
        service.updateJob();
    }

}
