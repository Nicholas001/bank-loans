package com.nicholas.runner;

import com.nicholas.customers.CustomersMeniu;
import com.nicholas.db.DBInitalization;
import com.nicholas.jobs.JobsMeniu;
import com.nicholas.loans.LoansMeniu;

import java.sql.SQLException;
import java.util.Scanner;

public class MainMeniu {

    CustomersMeniu customersMeniu;
    JobsMeniu jobsMeniu;
    LoansMeniu loansMeniu;

    public MainMeniu (){
        customersMeniu = new CustomersMeniu();
        jobsMeniu = new JobsMeniu();
        loansMeniu = new LoansMeniu();
    }


    public void mainMeniu() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        boolean test = true;

        while (test) {

            System.out.println("Please insert:\n" +
                    "\t 1. Loans meniu\n" +
                    "\t 2. Customers meniu\n" +
                    "\t 3. Jobs meniu\n " +
                    "\t 4. Exit ");

            int option = scanner.nextInt();
            switch (option) {
                case 1:
                    loansMeniu();
                    break;
                case 2:
                    customersMeniu();
                    break;
                case 3:
                    jobsMeniu();
                    break;
                case 4:
                    test = false;
                    break;
                default:
                    System.out.println("The selected option doesn`t exist! ");
                    System.out.println();
            }
        }
    }

    public void customersMeniu() throws SQLException {
        customersMeniu.customerMeniu();
    }

    public void jobsMeniu() throws SQLException {
        jobsMeniu.jobsMeniu();
    }

    public void loansMeniu() throws SQLException {
        loansMeniu.loanMeniu();
    }

}
