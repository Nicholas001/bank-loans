package com.nicholas.runner;

import com.nicholas.db.DBInitalization;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {

        DBInitalization initalization = new DBInitalization();
        initalization.initializeDatabase();
        MainMeniu mainMeniu = new MainMeniu();
        mainMeniu.mainMeniu();


    }
}
