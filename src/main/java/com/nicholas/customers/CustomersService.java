package com.nicholas.customers;

import com.nicholas.db.DbUtil;

import java.sql.*;
import java.util.Scanner;

public class CustomersService {

    public void displayAllCustomers() throws SQLException {

        Connection connection = DbUtil.getConnection();
        Statement statement;
        String quary = "SELECT * FROM customers;";
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(quary);
        System.out.println("Customers:\n");
        while (resultSet.next()) {
            int customerID = resultSet.getInt("Customer_ID");
            String name = resultSet.getString("Name");
            String address = resultSet.getString("Address");
            String details = resultSet.getString("Details");
            System.out.println("\t" + customerID + ": " + name + " - " + address + "; " + details + ";\n");
        }

        statement.close();
        connection.close();
    }

    public void insertNewCustomer() throws SQLException {

        Connection connection = DbUtil.getConnection();
        String quary = "SELECT customer_id FROM customers ORDER BY customer_id DESC LIMIT 1;";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(quary);
        int customer_id=0;
        while (resultSet.next()) {
            customer_id = resultSet.getInt("Customer_ID");}
        customer_id++;
        statement.close();

        System.out.println("Please insert data for the new customer: ");


        System.out.println("\t Customer name:");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        System.out.println("\t Cutomer address: ");
        String address = scanner.nextLine();

        System.out.println("\t Cutomer details: ");
        String details = scanner.nextLine();

        String sql = "INSERT INTO customers (customer_id, name, address, details ) VALUES (?, ?, ?, ? )";
        PreparedStatement statement2 = connection.prepareStatement(sql);

        statement2.setInt(1, customer_id);
        statement2.setString(2, name);
        statement2.setString(3, address);
        statement2.setString(4, details);

        statement2.execute();

        statement.close();
        connection.close();
        System.out.println("The new customer has been added!");
    }

    public void deleteCustomer() throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which customer do you what to remove? Plase insert customer id: ");
        int id = scanner.nextInt();

        Connection connection = DbUtil.getConnection();
        String sql = "DELETE FROM customers WHERE (Customer_ID = ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, id);

        statement.execute();

        statement.close();
        connection.close();

        System.out.println("Customer " + id + " was deleted! \n");

    }

    public void updateCustomer() throws SQLException {
        Connection connection = DbUtil.getConnection();
        System.out.println("Please insert customer id: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        scanner.nextLine();

        System.out.println("What do you want to change\n");
        System.out.println("\t1. Name \n\t2. Address \n\t3. Details\n");
        int option = scanner.nextInt();
        scanner.nextLine();
        switch (option) {
            case 1:
                String sql1 = "UPDATE customers SET Name = ? WHERE (Customer_ID = " + id + ");\n ";
                PreparedStatement statement1 = connection.prepareStatement(sql1);
                System.out.println("Plase enter the new name ");
                String name = scanner.nextLine();
                statement1.setString(1, name);
                statement1.execute();
                statement1.close();
                break;
            case 2:
                String sql2 = "UPDATE customers SET Address = ? WHERE (Customer_ID = " + id + ");\n ";
                PreparedStatement statement2 = connection.prepareStatement(sql2);
                System.out.println("Plase enter the new address ");
                String address = scanner.nextLine();
                statement2.setString(1, address);
                statement2.execute();
                statement2.close();
                break;
            case 3:
                String sql3 = "UPDATE customers SET Details = ? WHERE (Customer_ID = " + id + ");\n ";
                PreparedStatement statement3 = connection.prepareStatement(sql3);
                System.out.println("Plase enter the new details ");
                String details = scanner.nextLine();
                statement3.setString(1, details);
                statement3.execute();
                statement3.close();
                break;
            case 4:
                break;
            default:
                System.out.println("The selected option doesn`t exist! \n");
        }

        connection.close();
    }


}
