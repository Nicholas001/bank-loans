package com.nicholas.customers;

import java.sql.SQLException;
import java.util.Scanner;

public class CustomersMeniu {

    private CustomersService service;

    public CustomersMeniu () {
        service = new CustomersService();
    }

    public void customerMeniu() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        boolean test = true;

        while (test) {
            System.out.println("Please insert:\n" +
                    "\t 1. Display all customers\n" +
                    "\t 2. Insert new customer\n" +
                    "\t 3. Delete customer\n" +
                    "\t 4. Update customer\n"+
                    "\t 5. Return \n");

            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    displayAllCustomers();
                    break;
                case 2:
                    insertNewCustomer();
                    break;
                case 3:
                    deleteCustomer();
                    break;
                case 4:
                    updateCustomer();
                    break;
                case 5:
                    test = false;
                    break;
                default:
                    System.out.println("The selected option doesn`t exist! \n");
            }
        }
    }

    public void displayAllCustomers() throws SQLException {
        service.displayAllCustomers();
    }

    public void insertNewCustomer() throws SQLException {
        service.insertNewCustomer();
    }

    public void deleteCustomer() throws SQLException {
        service.deleteCustomer();
    }

    public void updateCustomer() throws SQLException{
        service.updateCustomer();
    }
}
