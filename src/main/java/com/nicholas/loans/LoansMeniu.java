package com.nicholas.loans;

import com.nicholas.customers.CustomersService;

import java.sql.SQLException;
import java.util.Scanner;

public class LoansMeniu {

    private LoansService service;

    public LoansMeniu () {
        service = new LoansService();
    }

    public void loanMeniu() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        boolean test = true;

        while (test) {
            System.out.println("Please insert:\n" +
                    "\t 1. Display all loans\n" +
                    "\t 2. Insert new loan\n" +
                    "\t 3. Delete loan\n" +
                    "\t 4. Update loan\n"+
                    "\t 5. Return \n");

            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    displayAllLoans();
                    break;
                case 2:
                    insertNewLoan();
                    break;
                case 3:
                    deleteLoan();
                    break;
                case 4:
                    updateLoan();
                    break;
                case 5:
                    test = false;
                    break;
                default:
                    System.out.println("The selected option doesn`t exist! \n");
            }
        }
    }

    public void displayAllLoans() throws SQLException {
        service.displayAllLoans();
    }

    public void insertNewLoan() throws SQLException {
        service.insertNewLoan();
    }

    public void deleteLoan() throws SQLException {
        service.deleteLoan();
    }

    public void updateLoan() throws SQLException{
        service.updateLoan();
    }

}
