package com.nicholas.loans;

import com.nicholas.db.DbUtil;

import java.sql.*;
import java.util.Scanner;

public class LoansService {


    public void displayAllLoans() throws SQLException {

        Connection connection = DbUtil.getConnection();
        Statement statement;
        String quary = "SELECT loans.Loan_ID, customers.Name, loans.Amount, loans.Purpose, " +
                "loans.Start_Date, loans.End_Date FROM loans " +
                "JOIN customers ON loans.Customer_ID = customers.Customer_ID;";
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(quary);
        System.out.println("Loans:\n");
        while (resultSet.next()) {
            int loanID = resultSet.getInt("Loan_ID");
            String name = resultSet.getString("Name");
            int amount = resultSet.getInt("Amount");
            String startDate = resultSet.getString("Start_date");
            String endDate = resultSet.getString("End_date");
            String purpose = resultSet.getString("Purpose");
            System.out.println("\t" + "ID: " + loanID + "; Name: " + name + "; Amount: " + amount +
                    "; Start date: " + startDate + " - End date: " + endDate + "; Purpose: " + purpose + ";\n");
        }
        statement.close();
        connection.close();
    }

    public void insertNewLoan() throws SQLException {

        Connection connection = DbUtil.getConnection();
        String quary = "SELECT loan_id FROM loans ORDER BY loan_id DESC LIMIT 1;";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(quary);
        int loanID = 0;
        while (resultSet.next()) {
            loanID = resultSet.getInt("Loan_ID");
        }
        loanID++;
        statement.close();

        System.out.println("Please insert data for the new loan: ");


        System.out.println("\t Customer ID:");
        Scanner scanner = new Scanner(System.in);
        int customerID = scanner.nextInt();
        scanner.nextLine();

        System.out.println("\t Loan purpose: ");
        String loanPurpose = scanner.nextLine();

        System.out.println("\t Amount: ");
        int amount = scanner.nextInt();
        scanner.nextLine();

        System.out.println("\t Start date: ");
        String startDate = scanner.nextLine();

        System.out.println("\t End date: ");
        String endDate = scanner.nextLine();


        String sql = "INSERT INTO loans (loan_id, customer_id, purpose, amount, start_date, end_date ) VALUES (?, ?, ?, ?, ?, ? )";
        PreparedStatement statement2 = connection.prepareStatement(sql);

        statement2.setInt(1, loanID);
        statement2.setInt(2, customerID);
        statement2.setString(3, loanPurpose);
        statement2.setInt(4, amount);
        statement2.setString(5, startDate);
        statement2.setString(6, endDate);

        statement2.execute();

        statement.close();
        connection.close();
        System.out.println("The new loan has been added!");
    }

    public void deleteLoan() throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which loan do you what to remove? Plase insert loan id: ");
        int id = scanner.nextInt();

        Connection connection = DbUtil.getConnection();
        String sql = "DELETE FROM loans WHERE (Loan_ID = ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, id);

        statement.execute();

        statement.close();
        connection.close();

        System.out.println("Loan " + id + " was deleted! \n");

    }

    public void updateLoan() throws SQLException {
        Connection connection = DbUtil.getConnection();
        System.out.println("Please insert loan id: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        scanner.nextLine();

        System.out.println("What do you want to change\n");
        System.out.println("\t1. Customer ID \n\t2. Purpose \n\t3. Amount\n\t4. Start date\n\t5. End Date\n");
        int option = scanner.nextInt();
        scanner.nextLine();
        switch (option) {
            case 1:
                String sql1 = "UPDATE loans SET customer_id = ? WHERE (loan_ID = " + id + ");\n ";
                PreparedStatement statement1 = connection.prepareStatement(sql1);
                System.out.println("Plase enter the new Customer ID ");
                int customerID = scanner.nextInt();
                statement1.setInt(1, customerID);
                statement1.execute();
                statement1.close();
                break;
            case 2:
                String sql2 = "UPDATE loans SET purpose = ? WHERE (loan_ID = " + id + ");\n ";
                PreparedStatement statement2 = connection.prepareStatement(sql2);
                System.out.println("Plase enter the new purpose ");
                String purpose = scanner.nextLine();
                statement2.setString(1, purpose);
                statement2.execute();
                statement2.close();
                break;
            case 3:
                String sql3 = "UPDATE loans SET amount = ? WHERE (loan_ID = " + id + ");\n ";
                PreparedStatement statement3 = connection.prepareStatement(sql3);
                System.out.println("Plase enter the new amount ");
                int amount = scanner.nextInt();
                statement3.setInt(1, amount);
                statement3.execute();
                statement3.close();
                break;
            case 4:
                String sql4 = "UPDATE loans SET start_date = ? WHERE (loan_ID = " + id + ");\n ";
                PreparedStatement statement4 = connection.prepareStatement(sql4);
                System.out.println("Plase enter the new start date ");
                String startDate = scanner.nextLine();
                statement4.setString(1, startDate);
                statement4.execute();
                statement4.close();
                break;
            case 5:
                String sql5 = "UPDATE loans SET end_date = ? WHERE (loan_ID = " + id + ");\n ";
                PreparedStatement statement5 = connection.prepareStatement(sql5);
                System.out.println("Plase enter the new end date ");
                String endDate = scanner.nextLine();
                statement5.setString(1, endDate);
                statement5.execute();
                statement5.close();
                break;
            default:
                System.out.println("The selected option doesn`t exist! \n");
        }

        connection.close();
    }

}
