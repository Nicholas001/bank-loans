package com.nicholas.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBInitalization {

    public void initializeDatabase() throws SQLException {
        DBInitalization dbInitalization = new DBInitalization();
        dbInitalization.createCustomersTable();
        dbInitalization.createJobsTable();
        dbInitalization.createLoansTable();
        dbInitalization.createCustomerXJobsTable();
    }

    public void createCustomersTable() throws SQLException{

        Connection connection = DbUtil.getConnection();
        String sql = "CREATE TABLE IF NOT EXISTS `bank_loan`.`customers` (\n" +
                "  `Customer_ID` INT NOT NULL,\n" +
                "  `Name` VARCHAR(45) NOT NULL,\n" +
                "  `Address` VARCHAR(45) NOT NULL,\n" +
                "  `Details` VARCHAR(45) NULL,\n" +
                "  PRIMARY KEY (`Customer_ID`),\n" +
                "  UNIQUE INDEX `Customer_ID_UNIQUE` (`Customer_ID` ASC) VISIBLE);";
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.execute();

        statement.close();
        connection.close();
    }

    public void createLoansTable() throws SQLException{

        Connection connection = DbUtil.getConnection();
        String sql = "CREATE TABLE IF NOT EXISTS `bank_loan`.`loans` (\n" +
                "  `Loan_ID` INT NOT NULL,\n" +
                "  `Customer_ID` INT NOT NULL,\n" +
                "  `Purpose` VARCHAR(45) NOT NULL,\n" +
                "  `Amount` INT NOT NULL,\n" +
                "  `Start_Date` DATE,\n" +
                "  `End_Date` DATE,\n" +
                "  PRIMARY KEY (`Loan_ID`),\n" +
                "  CONSTRAINT fk_customer_id_1 FOREIGN KEY (customer_id) REFERENCES customers (customer_id));\n";
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.execute();

        statement.close();
        connection.close();
    }

    public void createJobsTable() throws SQLException{

        Connection connection = DbUtil.getConnection();
        String sql = "CREATE TABLE IF NOT EXISTS `bank_loan`.`jobs` (\n" +
                "  `Job_ID` INT NOT NULL,\n" +
                "  `Title` VARCHAR(45) NOT NULL,\n" +
                "  `Income` INT NOT NULL,\n" +
                "  PRIMARY KEY (`Job_ID`),\n" +
                "  UNIQUE INDEX `Job_ID_UNIQUE` (`Job_ID` ASC) VISIBLE);";
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.execute();

        statement.close();
        connection.close();


    }    public void createCustomerXJobsTable() throws SQLException{

        Connection connection = DbUtil.getConnection();
        String sql = "CREATE TABLE IF NOT EXISTS `bank_loan`.`customer_x_jobs` (\n" +
                "  `Customer_ID` INT NOT NULL,\n" +
                "  `Job_ID` INT NOT NULL,\n" +
                "  PRIMARY KEY (`Customer_ID`, `Job_ID`), \n" +
                "CONSTRAINT fk_customer_id_2 FOREIGN KEY (customer_id) REFERENCES customers (customer_id),\n" +
                "CONSTRAINT fk_job_id FOREIGN KEY (job_id) REFERENCES jobs (job_id));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.execute();
        statement.close();

        connection.close();
    }

}
