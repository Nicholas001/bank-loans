package com.nicholas.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {

    static String username = "root";

    static String password = "root";

    static String dbUrl = "jdbc:mysql://localhost:3306/bank_loan?serverTimezone=UTC";



    public static Connection getConnection() throws SQLException {

        return DriverManager.getConnection(dbUrl, username, password);

    }


}
